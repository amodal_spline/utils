#!/usr/bin/env python
# Krishna Somandepalli - 12/28/2015
import numpy as np
import cv2
import cv2.cv as cv
from video import create_capture
from common import clock, draw_str


#####################   		Main program starts here

def get_flow_img(hsv, frame1_gs, gs_img):
	
	#hsv = np.zeros_like(frame1_gs)
	hsv[...,1] = 255

	flow = cv2.calcOpticalFlowFarneback(frame1_gs, gs_img, 0.5, 3, 9, 3, 5, 1.2, 0)
	#     frame1_gs = gs_img

	mag, ang = cv2.cartToPolar(flow[...,0], flow[...,1])
	hsv[...,0] = ang*180/np.pi/2
	hsv[...,2] = cv2.normalize(mag,None,0,255,cv2.NORM_MINMAX)
	flow_rgb = cv2.cvtColor(hsv,cv2.COLOR_HSV2BGR)
	flow_gs = cv2.cvtColor(flow_rgb, cv2.COLOR_BGR2GRAY)
					        
    	return flow_gs

def get_img_luma(img):
	frame_lum = 0.27*img[:,:,-1] + 0.67*img[:,:,1] + 0.06*img[:,:,0]
        lum_mean = np.mean(frame_lum.flatten())
	return lum_mean

if __name__ == '__main__':
    import sys, getopt, os

    #Load the scenes file

    scenes=[a.rstrip().split() for a in open(sys.argv[2]).readlines()]
    scene_times=[float(a[1]) for a in scenes]
    #print scene_times
    outname = str(sys.argv[2]).split('.')[0]
    
    #read videos
    filename = sys.argv[1]
    
    #Read the video file
    cam  = cv2.VideoCapture(filename)
    fps=cam.get(cv.CV_CAP_PROP_FPS)	#Read the file frame rate
    print fps

    frame_number=0
    current_scene_number=0
    frames_this_shot=0

    #color = np.random.randint(0,255,(1000,3))
    #feature_params = dict( maxCorners = 300, qualityLevel = 0.3, minDistance = 7, blockSize = 7 )
    #lk_params = dict(winSize = old_gray.shape, maxLevel = 2, criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))
    

    print len(scene_times)
    luma=[]
    intensity_avg=[]
    flow_intensity_avg=[]

    all_mean_lum=[]
    all_intensity_avg=[]
    all_flow_intensity_avg=[]
    
    frame_downsample_ref = 5
    while True:
    	frame_number+=1
	if current_scene_number==(len(scene_times)-1): break;
	#print frame_number,scene_times[current_scene_number]*fps
    	if ((frame_number>scene_times[current_scene_number]*fps) & (current_scene_number<len(scene_times)-1)):
	    current_scene_number+=1
	    
	    #Save stuff just after the shot
	    print current_scene_number, frames_this_shot
	    mean_lum = np.mean(luma)
	    all_mean_lum.append(mean_lum)

	    mean_intensity = np.mean(intensity_avg)
	    all_intensity_avg.append(mean_intensity)

	    if flow_intensity_avg:
	    	mean_flow_intensity = np.mean(flow_intensity_avg)
		all_flow_intensity_avg.append(mean_flow_intensity)
	    else: all_flow_intensity_avg.append(np.nan)
	    # Reinitialize for accumulation
	    frames_this_shot = 0
	    luma=[]
	    intensity_avg=[]
	    flow_intensity_avg=[]
        ret, img = cam.read()
	
        if not(ret):
    	    print "Skipping frame.."
    	    continue
	
	## Accumulate stuff for this shot
	#frames_this_shot+=1
	img_height, img_width, ch = img.shape
	hsv_flow = np.zeros_like(img)
	if frames_this_shot == 0:
		gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
		gray = cv2.equalizeHist(gray)
		flow_ref_img = gray.copy()

        	lum_mean = get_img_luma(img)
		## Compute per frame luminance
		luma.append(lum_mean)

		## Compute RGB intensity average
		img_mean = cv2.mean(gray)
		intensity_avg.append(img_mean)
	
	## Compute dense optical flow
	elif frames_this_shot==1:
		gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
		gray = cv2.equalizeHist(gray)
		flow_input_img = gray.copy()
		flow_gs = get_flow_img(hsv_flow, flow_ref_img, flow_input_img)
		#flow_ref_img = flow_input_img
		flow_intensity_avg.append(cv2.mean(flow_gs))

        	lum_mean = get_img_luma(img)
		## Compute per frame luminance
		luma.append(lum_mean)

		## Compute RGB intensity average
		img_mean = cv2.mean(gray)
		intensity_avg.append(img_mean)
	#!!! Downsampling by 5 - stats
	elif frames_this_shot>1:
		if not((frames_this_shot+1)%5):
			gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
			gray = cv2.equalizeHist(gray)
			flow_ref_img = gray.copy()

			lum_mean = get_img_luma(img)
			## Compute per frame luminance
			luma.append(lum_mean)

		## Compute RGB intensity average
			img_mean = cv2.mean(gray)
			intensity_avg.append(img_mean)
			frame_downsample_ref = frames_this_shot+1
			#print frame_downsample_ref

		if frames_this_shot == frame_downsample_ref:
			gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
			gray = cv2.equalizeHist(gray)
			flow_input_img = gray.copy()
			flow_gs = get_flow_img(hsv_flow, flow_ref_img, flow_input_img)
			#flow_ref_img = flow_input_img
			flow_intensity_avg.append(cv2.mean(flow_gs))

			lum_mean = get_img_luma(img)
			## Compute per frame luminance
			luma.append(lum_mean)

			## Compute RGB intensity average
			img_mean = cv2.mean(gray)
			intensity_avg.append(img_mean)
		#cv2.imshow('disp', flow_gs)
		#k = cv2.cv.WaitKey(30) & 0xff
	frames_this_shot+=1
	#cv2.destroyAllWindows()

#save all low level features
    np.save(outname+'_luma.npy',all_mean_lum)
    np.save(outname+'_intensity.npy', all_intensity_avg)
    np.save(outname+'_flow.npy', all_flow_intensity_avg)
